表1 STUDENT 学生个人信息表

|      |      |      |
| ---- | ---- | ---- |
|      |      |      |
|      |      |      |
|      |      |      |

![流程表](https://gitee.com/RocDe/picture/raw/master/img/20201219012414.png)



$$
e^{i\pi} + 1 = 0
$$





## 常用公式的代码

**上/下标**

| 算式   | Markdown |
| ------ | -------- |
| $x^2 $ | x^2      |
| $y_1 $ | y_1      |

**分式**

| 算式              | Markdown    |
| ----------------- | ----------- |
| $1/2$      | 1/2         |
| $\frac{1}{2}$ | \frac{1}{2} |

**省略号**

| 省略号    | Markdown |
| --------- | -------- |
| $\cdots$ | \cdots   |

**开根号**

| 算式        | Markdown |
| ----------- | -------- |
| $\sqrt{2}$ | \sqrt{2} |

**矢量**

| 算式          | Markdown |
| ------------- | -------- |
| $\vec{a}$ | \vec{a}  |

**积分**

| 算式                                      | Markdown          |
| ----------------------------------------- | ----------------- |
| $\int{x}dx$     | \int{x}dx         |
| $\int_{1}^{2}{x}dx$ | \int_{1}^{2}{x}dx |

**极限**

| 算式                                             | Markdown                   |
| ------------------------------------------------ | -------------------------- |
| $\lim{a+b}$                   | \lim{a+b}                  |
| $\lim_{n\rightarrow+\infty}$ | \lim_{n\rightarrow+\infty} |

**累加**

| 算式                                                 | Markdown              |
| ---------------------------------------------------- | --------------------- |
| $\sum{a} $                         | \sum{a}               |
| $\sum_{n=1}^{100}{a_n}$ | \sum_{n=1}^{100}{a_n} |

**累乘**

| 算式                                               | Markdown              |
| -------------------------------------------------- | --------------------- |
| $\prod{x}$       | \prod{x}              |
| $\prod_{n=1}^{99}{x_n}$| \prod_{n=1}^{99}{x_n} |

**希腊字母**

| 大写        | Markdown | 小写             | Markdown    |
| ----------- | -------- | ---------------- | ----------- |
| $A$     | A        | $\alpha$     | \alpha      |
| $B$      | B        | $\beta$      | \beta       |
| $\Gamma$   | \Gamma   | $\gamma$     | \gamma      |
| $\Delta$   | \Delta   | $\delta$     | \delta      |
| $E$     | E        | $\epsilon$   | \epsilon    |
|             |          | $\varepsilon$ | \varepsilon |
| $Z$     | Z        | $\zeta$      | \zeta       |
| $H$     | H        | $\eta$       | \eta        |
| $\Theta$   | \Theta   | $\theta$     | \theta      |
| $I $   | I        | $\iota$      | \iota       |
| $K$     | K        | $\kappa$     | \kappa      |
| $\Lambda$  | \Lambda  | $\lambda$    | \lambda     |
|$ M $    | M        | $\mu$        | \mu         |
| $N $      | N        | $\nu$        | \nu         |
|$\Xi$      | \Xi      | $\xi$        | \xi         |
| $O$      | O        | $\omicron$   | \omicron    |
| $\Pi$      | \Pi      | $\pi$      | \pi         |
| $P$    | P        | $\rho$       | \rho        |
| $ \Sigma$   | \Sigma   | $\sigma$     | \sigma      |
| $T $    | T        | $\tau$     | \tau        |
| $ \Upsilon$ | \Upsilon | $\upsilon$   | \upsilon    |
| $\Phi$    | \Phi     | $\phi$       | \phi        |
|             |          | $\varphi$    | \varphi     |
| $ X$     | X        | $\chi$       | \chi        |
| $\Psi$     | \Psi     | $\psi$       | \psi        |
| $\Omega$   | \Omega   | $\omega$     | \omega      |

**三角函数**

| 三角函数 | Markdown |
| -------- | -------- |
| $ \sin$  | \sin     |
| $\cos$   | \cos     |
| $\tan$   | \tan     |

**对数函数**

| 算式      | Markdown |
| --------- | -------- |
| $\ln2$    | \ln2     |
| $\log_28$ | \log_28  |
| $\lg10$   | \lg10    |

**关系运算符**

| 运算符   | Markdown |
| -------- | -------- |
| $\pm$    | \pm      |
| $\times$ | \times   |
| $\cdot$  | \cdot    |
| $\div$   | \div     |
| $\neq$   | \neq     |
| $\equiv$ | \equiv   |
| $\leq$   | \leq     |
| $\geq$   | \geq     |

**其它特殊字符**

| 符号         | Markdown   |
| ------------ | ---------- |
| $\forall$    | \forall    |
| $\infty$     | \infty     |
| $\emptyset$  | \emptyset  |
| $\exists$    | \exists    |
| $\nabla$     | \nabla     |
| $\bot$       | \bot       |
| $\angle$     | \angle     |
| $\because$   | \because   |
| $\therefore$ | \therefore |

#### 花括号

- 方法一

  代码
```
$$ f(x)=\left\{
\begin{aligned}
x & = & \cos(t) \\
y & = & \sin(t) \\
z & = & \frac xy
\end{aligned}
\right.
$$
```
效果
$$ f(x)=\left\{
\begin{aligned}
x & = & \cos(t) \\
y & = & \sin(t) \\
z & = & \frac xy
\end{aligned}
\right.
$$


- 方法二

  代码

```
$$ F^{HLLC}=\left\{
\begin{array}{rcl}
F_L       &      & {0      <      S_L}\\
F^*_L     &      & {S_L \leq 0 < S_M}\\
F^*_R     &      & {S_M \leq 0 < S_R}\\
F_R       &      & {S_R \leq 0}
\end{array} \right. $$
```
效果
$$ F^{HLLC}=\left\{
\begin{array}{rcl}
F_L       &      & {0      <      S_L}\\
F^*_L     &      & {S_L \leq 0 < S_M}\\
F^*_R     &      & {S_M \leq 0 < S_R}\\
F_R       &      & {S_R \leq 0}
\end{array} \right. $$

- 方法三

代码  

```
$$f(x)=
\begin{cases}
0& \text{x=0}\\
1& \text{x!=0}
\end{cases}$$
```
效果

$$f(x)=
\begin{cases}
0& \text{x=0}\\
1& \text{x!=0}
\end{cases}$$

